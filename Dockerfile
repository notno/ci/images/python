ARG KANIKO_TAG=latest
FROM python:${KANIKO_TAG}

RUN apt-get update -y && apt-get full-upgrade -y

COPY requirements.txt .
RUN pip install --no-cache-dir --upgrade -r requirements.txt
